﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SecureApp.Classes;

namespace SecureApp.Dao
{
    class GroupeDao : AbstractDao
    {

        public List<Groupe> GetGroupesFromUtilisateur(long id)
        {
            List<Groupe> groupes = new List<Groupe>();

            Connection.Open();

            string commandText = @"
                SELECT `groupe`.*
                    FROM `groupe`
                    INNER JOIN `utilisateur_groupe` ON `utilisateur_groupe`.`id_groupe` = `groupe`.`id`
                    WHERE `id_utilisateur` = ?id_utilisateur;
            ";

            MySqlCommand command = new MySqlCommand();
            command.CommandText = commandText;
            command.Connection = Connection;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_utilisateur", id));

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Groupe groupe = new Groupe();
                groupe.Id = reader.GetInt64("id");
                groupe.Nom = reader.GetString("nom");
                groupes.Add(groupe);
            }
            reader.Close();
            Connection.Close();

            return groupes;
        }

        public List<Groupe> GetNotAffectedGroupesFromUtilisateur(long id)
        {
            List<Groupe> groupes = new List<Groupe>();

            Connection.Open();

            string commandText = @"
                SELECT `groupe`.*
                    FROM `groupe`
                    WHERE `id` NOT IN
                    (
                        SELECT `id_groupe`
                        FROM `utilisateur_groupe`
                        WHERE `id_utilisateur` = ?id_utilisateur
                    );
            ";

            MySqlCommand command = new MySqlCommand();
            command.CommandText = commandText;
            command.Connection = Connection;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_utilisateur", id));

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Groupe groupe = new Groupe();
                groupe.Id = reader.GetInt64("id");
                groupe.Nom = reader.GetString("nom");
                groupes.Add(groupe);
            }
            reader.Close();
            Connection.Close();

            return groupes;
        }
    }
}
