﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SecureApp.Classes;

namespace SecureApp.Dao
{
    class DroitDao : AbstractDao
    {
        public List<Droit> GetDroitsFromGroupe(long id)
        {
            List<Droit> droits = new List<Droit>();

            Connection.Open();

            string commandText = @"
                SELECT `droit`.*
                    FROM `droit`
                    INNER JOIN `groupe_droit` ON `groupe_droit`.`id_droit` = `droit`.`id`
                    WHERE `id_groupe` = ?id_groupe;
            ";

            MySqlCommand command = new MySqlCommand();
            command.CommandText = commandText;
            command.Connection = Connection;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", id));

            MySqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                Droit droit = new Droit();
                droit.Id = reader.GetInt64("id");
                droit.Nom = reader.GetString("nom");
                droits.Add(droit);
            }
            reader.Close();
            Connection.Close();

            return droits;
        }

        public List<Droit> GetDroitsFromUtilisateur(long id)
        {
            List<Droit> droits = new List<Droit>();

            Connection.Open();

            string commandText = @"
                SELECT `droit`.*
                    FROM `droit`
                    INNER JOIN `groupe_droit` ON `groupe_droit`.`id_droit` = `droit`.`id`
                    INNER JOIN `utilisateur_groupe` ON `utilisateur_groupe`.`id_groupe` = `groupe_droit`.`id_groupe`
                    WHERE `id_utilisateur` = ?id_utilisateur;
            ";

            MySqlCommand command = new MySqlCommand();
            command.CommandText = commandText;
            command.Connection = Connection;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_utilisateur", id));

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Droit droit = new Droit();
                droit.Id = reader.GetInt64("id");
                droit.Nom = reader.GetString("nom");
                droits.Add(droit);
            }
            reader.Close();
            Connection.Close();

            return droits;
        }
    }
}
