﻿namespace SecureApp
{
    partial class FrmUserEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPseudo = new System.Windows.Forms.TextBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblPseudo = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.lblMotDePasse = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpGroupes = new System.Windows.Forms.GroupBox();
            this.lblNotAffected = new System.Windows.Forms.Label();
            this.lblAffected = new System.Windows.Forms.Label();
            this.lstNotAffected = new System.Windows.Forms.ListBox();
            this.lstAffected = new System.Windows.Forms.ListBox();
            this.btnAffect = new System.Windows.Forms.Button();
            this.btnDisaffect = new System.Windows.Forms.Button();
            this.grpGroupes.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPseudo
            // 
            this.txtPseudo.Location = new System.Drawing.Point(95, 12);
            this.txtPseudo.Name = "txtPseudo";
            this.txtPseudo.Size = new System.Drawing.Size(100, 20);
            this.txtPseudo.TabIndex = 0;
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(95, 67);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(100, 20);
            this.txtNom.TabIndex = 1;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(95, 93);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(100, 20);
            this.txtPrenom.TabIndex = 2;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(95, 119);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEmail.TabIndex = 3;
            // 
            // lblPseudo
            // 
            this.lblPseudo.AutoSize = true;
            this.lblPseudo.Location = new System.Drawing.Point(12, 15);
            this.lblPseudo.Name = "lblPseudo";
            this.lblPseudo.Size = new System.Drawing.Size(49, 13);
            this.lblPseudo.TabIndex = 4;
            this.lblPseudo.Text = "Pseudo :";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(12, 70);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(35, 13);
            this.lblNom.TabIndex = 5;
            this.lblNom.Text = "Nom :";
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(12, 96);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(49, 13);
            this.lblPrenom.TabIndex = 6;
            this.lblPrenom.Text = "Prénom :";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(12, 122);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 7;
            this.lblEmail.Text = "Email :";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(95, 38);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(75, 23);
            this.btnChangePassword.TabIndex = 8;
            this.btnChangePassword.Text = "Modifier ...";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // lblMotDePasse
            // 
            this.lblMotDePasse.AutoSize = true;
            this.lblMotDePasse.Location = new System.Drawing.Point(12, 43);
            this.lblMotDePasse.Name = "lblMotDePasse";
            this.lblMotDePasse.Size = new System.Drawing.Size(77, 13);
            this.lblMotDePasse.TabIndex = 9;
            this.lblMotDePasse.Text = "Mot de passe :";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(336, 155);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(421, 155);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpGroupes
            // 
            this.grpGroupes.Controls.Add(this.btnDisaffect);
            this.grpGroupes.Controls.Add(this.btnAffect);
            this.grpGroupes.Controls.Add(this.lstAffected);
            this.grpGroupes.Controls.Add(this.lstNotAffected);
            this.grpGroupes.Controls.Add(this.lblAffected);
            this.grpGroupes.Controls.Add(this.lblNotAffected);
            this.grpGroupes.Location = new System.Drawing.Point(201, 12);
            this.grpGroupes.Name = "grpGroupes";
            this.grpGroupes.Size = new System.Drawing.Size(295, 137);
            this.grpGroupes.TabIndex = 12;
            this.grpGroupes.TabStop = false;
            this.grpGroupes.Text = "Groupes";
            // 
            // lblNotAffected
            // 
            this.lblNotAffected.AutoSize = true;
            this.lblNotAffected.Location = new System.Drawing.Point(6, 16);
            this.lblNotAffected.Name = "lblNotAffected";
            this.lblNotAffected.Size = new System.Drawing.Size(75, 13);
            this.lblNotAffected.TabIndex = 0;
            this.lblNotAffected.Text = "Non-Affecté(s)";
            // 
            // lblAffected
            // 
            this.lblAffected.AutoSize = true;
            this.lblAffected.Location = new System.Drawing.Point(161, 16);
            this.lblAffected.Name = "lblAffected";
            this.lblAffected.Size = new System.Drawing.Size(52, 13);
            this.lblAffected.TabIndex = 1;
            this.lblAffected.Text = "Affecté(s)";
            // 
            // lstNotAffected
            // 
            this.lstNotAffected.FormattingEnabled = true;
            this.lstNotAffected.Location = new System.Drawing.Point(9, 32);
            this.lstNotAffected.Name = "lstNotAffected";
            this.lstNotAffected.Size = new System.Drawing.Size(120, 95);
            this.lstNotAffected.TabIndex = 2;
            // 
            // lstAffected
            // 
            this.lstAffected.FormattingEnabled = true;
            this.lstAffected.Location = new System.Drawing.Point(164, 31);
            this.lstAffected.Name = "lstAffected";
            this.lstAffected.Size = new System.Drawing.Size(120, 95);
            this.lstAffected.TabIndex = 3;
            // 
            // btnAffect
            // 
            this.btnAffect.Location = new System.Drawing.Point(135, 53);
            this.btnAffect.Name = "btnAffect";
            this.btnAffect.Size = new System.Drawing.Size(23, 23);
            this.btnAffect.TabIndex = 4;
            this.btnAffect.Text = ">";
            this.btnAffect.UseVisualStyleBackColor = true;
            this.btnAffect.Click += new System.EventHandler(this.btnAffect_Click);
            // 
            // btnDisaffect
            // 
            this.btnDisaffect.Location = new System.Drawing.Point(135, 82);
            this.btnDisaffect.Name = "btnDisaffect";
            this.btnDisaffect.Size = new System.Drawing.Size(23, 23);
            this.btnDisaffect.TabIndex = 5;
            this.btnDisaffect.Text = "<";
            this.btnDisaffect.UseVisualStyleBackColor = true;
            this.btnDisaffect.Click += new System.EventHandler(this.btnDisaffect_Click);
            // 
            // FrmUserEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 185);
            this.Controls.Add(this.grpGroupes);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblMotDePasse);
            this.Controls.Add(this.btnChangePassword);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblPrenom);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.lblPseudo);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtPrenom);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.txtPseudo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUserEdit";
            this.Text = "Utilisateur";
            this.Load += new System.EventHandler(this.FrmUserEdit_Load);
            this.grpGroupes.ResumeLayout(false);
            this.grpGroupes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPseudo;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblPseudo;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.Label lblMotDePasse;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox grpGroupes;
        private System.Windows.Forms.Button btnAffect;
        private System.Windows.Forms.ListBox lstAffected;
        private System.Windows.Forms.ListBox lstNotAffected;
        private System.Windows.Forms.Label lblAffected;
        private System.Windows.Forms.Label lblNotAffected;
        private System.Windows.Forms.Button btnDisaffect;
    }
}